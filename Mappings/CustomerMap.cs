﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Model;

namespace Mappings
{
    public class CustomerMap : ClassMap<Customer>
    {
        public CustomerMap()
        {
            Id(c => c.Id, "cid");
            Component(c => c.Name);
            Map(c => c.StreetAddress);
            Map(c => c.EmailAddress);
            Map(c => c.Password);
            References(c => c.ZipCode, "ZipCode").Cascade.All();
            HasMany<Rental>(c => c.Rentals)
                .KeyColumn("cid").Cascade.All();           
            HasManyToMany<Store>(c => c.PreferredStores)
                .ParentKeyColumn("cid")
                .ChildKeyColumn("StoreID")
                .Table("Prefers")
                .AsList(index => index.Column("StoreOrder"))
                .Cascade.All();
        }
    }
}
