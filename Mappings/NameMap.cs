﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Model;

namespace Mappings
{
    public class NameMap : ComponentMap<Name>
    {
        public NameMap()
        {
            Map(x => x.Title);
            Map(x => x.First, "FirstName");
            Map(x => x.Last, "LastName");
        }
    }
}
