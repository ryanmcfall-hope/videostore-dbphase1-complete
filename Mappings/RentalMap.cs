﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Model;

namespace Mappings
{
    public class RentalMap : ClassMap<Rental>
    {
        public RentalMap()
        {
            Id(r => r.Id, "RentalID");
            Map(r => r.DueDate);
            Map(r => r.RentalDate);
            Map(r => r.ReturnDate);
            References(r => r.Video, "VideoID").Cascade.All();
            References(r => r.Customer, "cid").Cascade.All();
        }
    }
}
