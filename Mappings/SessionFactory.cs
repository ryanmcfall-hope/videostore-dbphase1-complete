﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using Migrations;
using NHibernate;

namespace Mappings
{
    public class SessionFactory
    {
        public static ISessionFactory CreateSessionFactory()
        {
            var builder = new SqlConnectionStringBuilder();
            builder["Data Source"] = "sql.cs.hope.edu";
            builder["uid"] = "ryan.mcfall";
            builder["password"] = "000000000";
            builder["Integrated Security"] = false;
            builder["Initial Catalog"] = "ryan.mcfall";
            return Fluently.Configure()
              .Database(MsSqlConfiguration.MsSql2012.ConnectionString(builder.ConnectionString).DefaultSchema(Names.Schema).ShowSql())
              .Mappings(m => m.FluentMappings.AddFromAssemblyOf<ZipCodeMap>())
              .CurrentSessionContext<NHibernate.Context.ThreadLocalSessionContext>()
              .BuildSessionFactory();
        }
    }
}
