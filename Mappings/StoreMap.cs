﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Model;

namespace Mappings
{
    public class StoreMap : ClassMap<Store>
    {
        public StoreMap()
        {
            Id(s => s.Id, "StoreID");
            Map(s => s.StreetAddress);
            Map(s => s.PhoneNumber);
            References(s => s.ZipCode, "ZipCode").Cascade.All();
            HasMany<Video>(s => s.Videos).KeyColumn("StoreID").Cascade.All();
        }
    }
}
