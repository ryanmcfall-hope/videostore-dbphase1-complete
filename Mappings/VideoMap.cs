﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Model;

namespace Mappings
{
    public class VideoMap : ClassMap<Video>
    {
        public VideoMap()
        {
            Id(v => v.Id, "VideoID");            
            Map(v => v.NewArrival);
            Map(v => v.PurchaseDate, "DatePurchased");
            References(v => v.Store, "StoreID").Cascade.All();            
        }
    }
}
