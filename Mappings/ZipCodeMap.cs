using FluentNHibernate.Mapping;
using Model;

namespace Mappings
{
    public class ZipCodeMap : ClassMap<ZipCode>
    {
        public ZipCodeMap()
        {
            Id(z => z.Code).GeneratedBy.Assigned();
            Map(z => z.City);
            Map(z => z.State);
        }
    }
}