﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using FluentNHibernate.Testing;
using Mappings;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Migrations;
using Model;
using NHibernate;
using Area = Model.Area;
using Customer = Model.Customer;
using Rental = Model.Rental;
using Store = Model.Store;
using Video = Model.Video;
using ZipCode = Model.ZipCode;

namespace MappingsTest
{
    [TestClass]
    public class TestMappings
    {
        private ISession _session;
        private ZipCode _holland;
        private ZipCode _zeeland;
        private Store _chicagoDriveStore;
        private Store _zeelandStore;
        private Video _video;

        [TestInitialize]
        public void SetUp()
        {
            _session = SessionFactory.CreateSessionFactory().GetCurrentSession();

            _session.CreateSQLQuery("delete from " + Names.Schema + ".Prefers").ExecuteUpdate();
            _session.CreateSQLQuery("delete from " + Names.Schema + ".Rental").ExecuteUpdate();
            _session.CreateSQLQuery("delete from " + Names.Schema + ".ZipCode").ExecuteUpdate();
            _session.CreateSQLQuery("delete from " + Names.Schema + ".Area").ExecuteUpdate();            
            _session.CreateSQLQuery("delete from " + Names.Schema + ".Customer").ExecuteUpdate();
            _session.CreateSQLQuery("delete from " + Names.Schema + ".Video").ExecuteUpdate();
            _session.CreateSQLQuery("delete from " + Names.Schema + ".Store").ExecuteUpdate();

            _holland = new ZipCode() { Code = "49423", City = "Holland", State = "Michigan" };
            _zeeland = new ZipCode() { Code = "49464", City = "Zeeland", State = "Michigan" };

            _chicagoDriveStore = new Store()
            {
                StreetAddress = "123 Chicago Drive",
                PhoneNumber = "616-394-1234",
                ZipCode = _holland
            };

            _zeelandStore = new Store()
            {
                StreetAddress = "456 Main St",
                PhoneNumber = "616-772-9876",
                ZipCode = _zeeland
            };

            _video = new Video()
            {
                NewArrival = true,
                PurchaseDate = DateTime.Now,
                Store = _zeelandStore
            };
        }        

        [TestMethod]
        public void TestZipCodeMap()
        {
            new PersistenceSpecification<ZipCode>(_session)
                .CheckProperty(z => z.Code, "49464")
                .CheckProperty(z => z.City, "Zeeland")
                .CheckProperty(z => z.State, "Michigan")
                .VerifyTheMappings();
        }

        [TestMethod]
        public void TestAreaMap()
        {
            new PersistenceSpecification<Area>(_session)
                .CheckProperty(a => a.Name, "Holland")
                .CheckList(a => a.ZipCodes, new List<ZipCode>() {_holland, _zeeland},
                    (area, zipcode) => area.AddZipCode(zipcode)
                )
                .VerifyTheMappings();
        }

        [TestMethod]
        public void TestCustomerMap()
        {
            //  Note that we're not testing that the rentals collection is mapped here, because it doesn't fit the same
            //  pattern of CheckList that the other classes do.  Instead, it's checked separately in the method named
            //  TestRentalsInCustomerMap below
            new PersistenceSpecification<Customer>(_session)
                .CheckProperty(c => c.Name,
                    new Name() {Title = "Dr.", First = "Ryan", Last = "McFall"})
                .CheckProperty(c => c.EmailAddress, "test@hope.edu")
                .CheckProperty(c => c.Password, "Pas$word1")
                .CheckProperty(c => c.StreetAddress, "27 Graves Pl")
                .CheckReference(c => c.ZipCode, _holland)
                .CheckList(c=>c.PreferredStores, 
                    new List<Store>()
                    {
                        _chicagoDriveStore, _zeelandStore
                    }
                )
                .VerifyTheMappings();            
        }

        [TestMethod]
        public void TestRentalsInCustomerMap()
        {
            var newCust = new Customer()
            {
                Name = new Name() {Title = "Dr.", First = "Ryan", Last = "McFall"},
                EmailAddress = "test@hope.edu",
                Password = "Pas$word1",
                StreetAddress = "27 Graves Pl",
                ZipCode = _holland
            };
            Rental rental = newCust.Rent(_video);
            rental.ReturnDate = null;
            _session.Save(newCust);
            _session.Refresh(newCust);

            Assert.AreEqual(1, newCust.Rentals.Count);  
            Assert.AreEqual(rental, newCust.Rentals[0]);          
        }

        [TestMethod]
        public void TestStoreMap()
        {
            new PersistenceSpecification<Store>(_session)
                .CheckProperty(s => s.StreetAddress, "27 Graves Place")
                .CheckProperty(s => s.PhoneNumber, "616-395-7510")
                .CheckReference(s => s.ZipCode, _holland)
                .CheckList(s => s.Videos,
                    new List<Video>()
                    {
                        _video
                    },
                    (store, video) => store.AddVideo(video)
                )
                .VerifyTheMappings();
        }

        [TestMethod]
        public void TestVideoMap()
        {
            new PersistenceSpecification<Video>(_session, new DateEqualityComparer())
                .CheckProperty(v => v.NewArrival, true)
                .CheckProperty(v => v.PurchaseDate, DateTime.Now)
                .CheckReference(v => v.Store, _chicagoDriveStore)
                .VerifyTheMappings();
        }

        [TestMethod]
        public void TestRentalMap()
        {
            new PersistenceSpecification<Rental>(_session, new DateEqualityComparer())
                .CheckProperty(r => r.RentalDate, DateTime.Now)
                .CheckProperty(r => r.DueDate, DateTime.Now.AddDays(7))
                .CheckProperty(r => r.ReturnDate, DateTime.Now.AddDays(6))
                .CheckReference(r => r.Customer, new Customer()
                {
                    Name = new Name() {First = "Ryan", Middle = "L", Last = "McFall"},
                    StreetAddress = "27 Graves Place",
                    EmailAddress = "mcfall@hope.edu",
                    Password = "pas$w0rD",
                    Phone = "616-395-7952",
                    ZipCode = _holland
                })
                .CheckReference(r => r.Video, _video)
                .VerifyTheMappings();
        }
    }

    public class DateEqualityComparer : IEqualityComparer
    {
        public bool Equals(object x, object y)
        {
            if (x == null || y == null) return false;

            if (x is DateTime && y is DateTime)
            {
                var xDateTime = (DateTime) x;
                var yDateTime = (DateTime) y;

                return xDateTime.Year == yDateTime.Year &&
                       xDateTime.Month == yDateTime.Month &&
                       xDateTime.Day == yDateTime.Day &&
                       xDateTime.Hour == yDateTime.Hour &&
                       xDateTime.Minute == yDateTime.Minute &&
                       xDateTime.Second == yDateTime.Second;

            }

            return x.Equals(y);
        }

        public int GetHashCode(object obj)
        {
            throw new NotImplementedException();
        }
    }
}
