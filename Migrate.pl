die "Usage: Migrate [up|down|rollback]" if (@ARGV != 1 && @ARGV != 2);

$task = $ARGV[0];
lc ($task);

die "Must specify argument of either up, down or rollback"
  if ($task ne "up" && $task ne "down" && $task ne "rollback");

if ($task eq "rollback") {
  die "Must specify version for rollback" if (@ARGV != 2);
  $task = "--version $ARGV[1] --task rollback:toversion";
}
else {
  $task = "--task migrate:$task";
}

$packageDir = "packages";
opendir (projectDir, $packageDir) or die "Cannot opendir $packageDir";
@FILES = readdir(projectDir);

@results = grep (/FluentMigrator\.Tools/, @FILES);
die "Can't find FluentMigrator.Tools in $packageDir" if (@results == 0) ;

$FluentMigrator = $results[0];

$server="sql.cs.hope.edu";
$username="ryan.mcfall";
$password="000000000";
$catalog="ryan.mcfall";

$integratedSecurity="false";

#$server="(LocalDB)\\v11.0";
#$catalog=$ENV{USERNAME};
#$integratedSecurity="true";

$cmd = "./packages/$FluentMigrator/tools/AnyCPU/40/Migrate" .
" --conn \"server=$server;password=$password;uid=$username;Trusted_Connection=yes;Integrated Security=$integratedSecurity;Initial Catalog=$catalog\"" .
" -provider sqlserver2008 --assembly Migrations\\bin\\Debug\\Migrations.dll $task --verbose true";

print "Executing $cmd\n";

system($cmd);
