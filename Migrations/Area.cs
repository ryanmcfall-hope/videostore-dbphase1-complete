﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Migrations
{
    [Migration(2)]
    public class Area : Migration
    {
        public override void Up()
        {
            Create.Table("Area").InSchema(Names.Schema)
                .WithColumn("AreaID").AsInt64().Identity().PrimaryKey()
                .WithColumn("Name").AsString(50).NotNullable().Unique();
        }

        public override void Down()
        {
            Delete.Table("Area").InSchema(Names.Schema);
        }
    }
}
