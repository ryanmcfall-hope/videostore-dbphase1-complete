﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Migrations
{
    [Migration(4)]
    public class Customer : Migration
    {
        public override void Up()
        {
            Create.Table("Customer").InSchema(Names.Schema)
                .WithColumn("cid").AsInt64().Identity().PrimaryKey()
                .WithColumn("Title").AsString(16).Nullable()
                .WithColumn("FirstName").AsString(50).NotNullable()                
                .WithColumn("LastName").AsString(50).NotNullable()
                .WithColumn("StreetAddress").AsString(255).NotNullable()
                .WithColumn("EmailAddress").AsString(128).Nullable()
                .WithColumn("Password").AsString(32).NotNullable()
                .WithColumn("ZipCode").AsString(10).NotNullable();

            Create.ForeignKey("CustomerToZipCode")
                .FromTable("Customer").InSchema(Names.Schema)
                .ForeignColumn("ZipCode")
                .ToTable("ZipCode").InSchema(Names.Schema)
                .PrimaryColumn("Code")
                .OnDeleteOrUpdate(Rule.Cascade);
        }

        public override void Down()
        {
            Delete.ForeignKey("CustomerToZipCode").OnTable("Customer").InSchema(Names.Schema);
            Delete.Table("Customer").InSchema(Names.Schema);
        }
    }
}
