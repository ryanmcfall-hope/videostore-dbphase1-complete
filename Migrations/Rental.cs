﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Migrations
{
    [Migration(9)]
    public class Rental : Migration
    {
        public override void Up()
        {
            Create.Table("Rental").InSchema(Names.Schema)
                .WithColumn("RentalID").AsInt64().Identity().PrimaryKey()
                .WithColumn("VideoID").AsInt64().NotNullable()
                .WithColumn("cid").AsInt64().NotNullable()
                .WithColumn("RentalDate").AsDateTime().NotNullable()
                .WithColumn("DueDate").AsDateTime().NotNullable()
                .WithColumn("ReturnDate").AsDateTime().Nullable();

            Create.UniqueConstraint("RentalCustAndVideo")
                .OnTable("Rental").WithSchema(Names.Schema)
                .Columns(new[] {"VideoID", "cid"});

            Create.ForeignKey("RentalToCustomer")
                .FromTable("Rental").InSchema(Names.Schema)
                .ForeignColumn("cid")
                .ToTable("Customer").InSchema(Names.Schema)
                .PrimaryColumn("cid")
                .OnDeleteOrUpdate(Rule.None);

            Create.ForeignKey("RentalToVideo")
                .FromTable("Rental").InSchema(Names.Schema)
                .ForeignColumn("VideoID")
                .ToTable("Video").InSchema(Names.Schema)
                .PrimaryColumn("VideoID")
                .OnDeleteOrUpdate(Rule.None);
        }

        public override void Down()
        {
            Delete.ForeignKey("RentalToVideo").OnTable("Rental").InSchema(Names.Schema);
            Delete.ForeignKey("RentalToCustomer").OnTable("Rental").InSchema(Names.Schema);
            Delete.Table("Rental").InSchema(Names.Schema);
        }
    }
}
