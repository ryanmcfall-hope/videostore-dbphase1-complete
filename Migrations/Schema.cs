﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Migrations
{
    [Migration(0)]
    public class Schema : Migration
    {
        public override void Up()
        {
            Create.Schema(Names.Schema);
        }

        public override void Down()
        {
            Delete.Schema(Names.Schema);
        }
    }
}
