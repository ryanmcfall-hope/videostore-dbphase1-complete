﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Migrations
{
    [Migration(5)]
    public class Store : Migration
    {
        public override void Up()
        {
            Create.Table("Store").InSchema(Names.Schema)
                .WithColumn("StoreID").AsInt64().Identity().PrimaryKey()
                .WithColumn("StreetAddress").AsString(255).NotNullable()
                .WithColumn("PhoneNumber").AsString(12).NotNullable()
                .WithColumn("ZipCode").AsString(10).NotNullable();

            Create.ForeignKey("StoreToZipCode")
                .FromTable("Store").InSchema(Names.Schema)
                .ForeignColumn("ZipCode")
                .ToTable("ZipCode").InSchema(Names.Schema)
                .PrimaryColumn("Code")
                .OnDeleteOrUpdate(Rule.Cascade);
        }

        public override void Down()
        {
            Delete.ForeignKey("StoreToZipCode").OnTable("Store").InSchema(Names.Schema);
            Delete.Table("Store").InSchema(Names.Schema);
        }
    }
}
