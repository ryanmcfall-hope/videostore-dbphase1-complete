﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Migrations
{
    [Migration(1)]
    public class ZipCode : Migration
    {
        public override void Up()
        {
            Create.Table("ZipCode").InSchema(Names.Schema)
                .WithColumn("Code").AsString(10).PrimaryKey()
                .WithColumn("City").AsString(100).NotNullable()
                .WithColumn("State").AsString(50).NotNullable();
        }

        public override void Down()
        {
            Delete.Table("ZipCode").InSchema(Names.Schema);
        }
    }
}
