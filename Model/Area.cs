﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Area : BaseEntity
    {
        public virtual string Name { get; set; }
        public virtual ISet<ZipCode> ZipCodes { get; protected internal set; }

        public Area() : base()
        {
            ZipCodes = new HashSet<ZipCode>();
        }

        public virtual void RemoveZipCode(ZipCode code)
        {
            if (!ZipCodes.Remove(code))
            {
                var message = string.Format(
                    "RemoveZipCode: The zip code with code {0} does not exist in the area with name {1}",
                    code.Code, this.Name
                );
                throw new ArgumentException(message);
            }
        }

        public virtual void AddZipCode(ZipCode code)
        {
            ZipCodes.Add(code);
        }
    }
}
