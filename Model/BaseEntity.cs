﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public abstract class BaseEntity
    {
        public virtual int Id { get; protected internal set; }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            
            
            if (GetType() != obj.GetType() && !obj.GetType().IsSubclassOf(GetType())) return false;

            var other = (BaseEntity) obj;
            return other.Id == Id;
        }

        public override int GetHashCode()
        {
            return (GetType().Name + Id).GetHashCode();
        }
    }
}
