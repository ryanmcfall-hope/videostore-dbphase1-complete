﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public enum TimeUnit { Day, Week, Month, Year}

    public class CommunicationMethod 
    {
        public virtual string Name { get; set; }
        public virtual int Frequency { get; set; }
        public virtual TimeUnit Units { get; set; }
        public virtual ISet<Customer> Customers { get; protected internal set; }

        public CommunicationMethod() : base()
        {
            Customers = new HashSet<Customer>();
        }

        public override bool Equals(object obj)
        {
            var other = obj as CommunicationMethod;
            if (other == null) return false;

            return Name.Equals(other.Name);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
    }
}
