using System;
using System.Collections.Generic;
using System.Linq;

namespace Model
{
    public class Customer : BaseEntity
    {
        public virtual Reservation Reservation { get; set; }

        public virtual Name Name { get; set; }

        private string _password;
        public virtual string Password
        {
            get
            {
                return _password;
            }

            set
            {
                if (value.Length < 6)
                {
                    throw new ArgumentException("Password is too short.  It must be at least 6 characters long.");
                }

                if (!value.Any(char.IsDigit))
                {
                    throw new ArgumentException("The password must contain at least 1 number.");
                }

                if (!value.Any(char.IsLower))
                {
                    throw new ArgumentException("The password must contain at least 1 lowercase letter.");
                }

                if (!value.Any(char.IsUpper))
                {
                    throw new ArgumentException("The password must contain at least 1 uppercase letter.");
                }

                _password = value;
            }
        }

        public virtual string EmailAddress { get; set; }
        public virtual string StreetAddress { get; set; }
        public virtual string Phone { get; set; }
        public virtual ZipCode ZipCode { get; set; }

        public virtual ISet<CommunicationMethod> CommunicationTypes { get; protected internal set; }
        public virtual IList<Rental> Rentals { get; protected set; }
        public virtual IList<Store> PreferredStores { get; protected set; }

        public virtual string FullName
        {
            get { return Name.First + " " + Name.Last; }
        }

        public Customer() : base()
        {
            CommunicationTypes = new HashSet<CommunicationMethod>();  
            Rentals = new List<Rental>();
            PreferredStores = new List<Store>(8);
        }

        public virtual void Allow(CommunicationMethod method)
        {
            CommunicationTypes.Add(method);
            method.Customers.Add(this);
        }

        public virtual void Deny(CommunicationMethod method)
        {
            CommunicationTypes.Remove(method);
            method.Customers.Remove(this);
        }

        public virtual Reservation AddReservation(Movie movie)
        {
            return movie.AddReservation(this);            
        }

        public virtual Rental Rent(Video video)
        {
            var rental = new Rental(this, video);
            Rentals.Add(rental);
            return rental;
        }

        public virtual List<Rental> LateRentals()
        {
            var rentals = Rentals as List<Rental>;
            var late = rentals.FindAll(x => !x.ReturnDate.HasValue && x.DueDate < DateTime.Now);
            return new List<Rental>(late.OrderBy(r=>r.DueDate));
        }
    }
}