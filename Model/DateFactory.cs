﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public enum DateFactoryMode
    {
        Test,
        Production
    }

    public class DateFactory
    {
        public static DateFactoryMode Mode { get; set; }

        public static readonly DateTime TestDate = new DateTime(2015,1,1,0,0,0);

        static DateFactory()
        {
            Mode = DateFactoryMode.Production;
        }

        public static DateTime CurrentDate
        {
            get
            {
                if (Mode == DateFactoryMode.Test)
                {
                    return TestDate;
                }
                else
                {
                    return DateTime.Now;
                }                
            }
        }


    }
}
