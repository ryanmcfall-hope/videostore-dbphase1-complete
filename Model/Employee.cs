using System;

namespace Model
{
    public class Employee : BaseEntity
    {
        public virtual Name Name { get; set; }
        public virtual DateTime DateHired { get; set; }
        public virtual string Username { get; set; }
        public virtual string Password { get; set; }
        public virtual Store Store { get; set; }

        private DateTime _birthDate;
        public virtual DateTime BirthDate
        {
            get
            {
                return _birthDate;
            }

            set
            {
                var sixteen = BirthDate.AddYears(16);
                if (value.CompareTo(sixteen) < 0)
                {
                    throw new ArgumentException(
                        string.Format("Cannot set birthdate to {0} for employee {1}; would make employee under sixteen at date of hire {2}", value, Name, DateHired)
                    );
                }
                _birthDate = value;
            }
        }

        public virtual bool IsManager
        {
            get { return Store != null && Store.Managers.Contains(this); }
        }

        private Employee _supervisor;

        public virtual Employee Supervisor
        {
            get {  return _supervisor;}
            set {
                if (value != null && value.Equals(this))
                {
                    throw new ArgumentException(
                        String.Format("Invalid attempt to set employee {0} as supervisor for employee {1}", value, this)
                    );
                }
                else
                {
                    _supervisor = value;    
                }
            }
        }

        public Employee()
        {
            DateHired = DateFactory.CurrentDate;
        }
    }
}