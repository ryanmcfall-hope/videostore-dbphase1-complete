﻿using System.Security.Policy;

namespace Model
{
    public class Genre
    {
        public virtual string Name { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as Genre;
            if (other != null)
            {
                return Name.Equals(other.Name);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
    }
}