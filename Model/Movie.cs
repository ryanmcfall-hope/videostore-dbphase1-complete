﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Movie : BaseEntity 
    {
        public virtual string MovieTitle { get; set; }
        public virtual int Year { get; set; }
        public virtual int RunningTimeInMinutes { get; set; }
        public virtual string Director { get; set; }
        public virtual string Language { get; set; }
        public virtual string Rating { get; set; }
        public virtual Genre PrimaryGenre { get; set; }
        public virtual IList<Reservation> Reservations { get; protected set; }

        public Movie()
        {
            Reservations = new List<Reservation>(10);
        }

        public virtual Reservation AddReservation(Customer customer)
        {
            var reservation = new Reservation()
            {
                Customer = customer,
                Movie = this
            };

            Reservations.Add(reservation);
            customer.Reservation = reservation;
            return reservation;
        }
    }
}
