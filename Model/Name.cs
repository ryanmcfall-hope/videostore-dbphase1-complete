﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Name
    {
        public string Title { get; set; }
        public string First { get; set; }
        public string Middle { get; set; }
        public string Last { get; set; }
        public string Suffix { get; set; }

        public override string ToString()
        {
            var builder = new StringBuilder(256);
            var space = "";

            if (Title != null)
            {
                builder.Append(Title);
                space = " ";
            }

            if (First != null)
            {
                builder.Append(space);
                builder.Append(First);
                space = " ";
            }

            if (Middle != null)
            {
                builder.Append(space);
                builder.Append(Middle);
                space = " ";
            }

            if (Last != null)
            {
                builder.Append(space);
                builder.Append(Last);
                space = " ";
            }

            if (Suffix!= null)
            {
                builder.Append(space);
                builder.Append(Suffix);             
            }

            return builder.ToString();
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var other = obj as Name;
            if (other == null) return false;

            if (Title == null)
            {
                if (other.Title != null) return false;
            }
            else
            {
                if (other.Title == null) return false;
                if (!Title.Equals(other.Title)) return false;
            }

            if (First == null)
            {
                if (other.First != null) return false;
            }
            else
            {
                if (other.First == null) return false;
                if (!First.Equals(other.First)) return false;
            }

            if (Middle == null)
            {
                if (other.Middle != null) return false;
            }
            else
            {
                if (other.Middle == null) return false;
                if (!Middle.Equals(other.Middle)) return false;
            }

            if (Last == null)
            {
                if (other.Last != null) return false;
            }
            else
            {
                if (other.Last == null) return false;
                if (!Last.Equals(other.Last)) return false;
            }

            if (Suffix == null)
            {
                if (other.Suffix != null) return false;
            }
            else
            {
                if (other.Suffix == null) return false;
                if (!Suffix.Equals(other.Suffix)) return false;
            }

            return true;
        }
    }
}
