using System;

namespace Model
{
    public class Rating : BaseEntity
    {
        public virtual string Comment { get; set; }

        private int _score;
        public const int MinScore = 1;
        public const int MaxScore = 5;

        public virtual int Score
        {
            get { return _score; }
            set
            {
                if (value < MinScore)
                {
                    throw new ArgumentException(
                        string.Format("Value for Rating Score must be at least {0}; value given was {1}", MinScore, value)
                    );
                }
                if (value > MaxScore)
                {
                    throw new ArgumentException(
                        string.Format("Value for Rating Score must be no more than {0}; value given was {1}", MaxScore, value)
                    );
                }

                _score = value;
            }
        }

        protected Rating() : base ()
        {
            
        }

        public Rating(int score)
        {
            Score = score;
        }

        public Rating(int score, string comment) : this (score)
        {
            Comment = comment;
        }
    }
}