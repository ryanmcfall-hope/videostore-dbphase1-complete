using System;

namespace Model
{
    public class Rental : BaseEntity
    {
        public virtual Video Video { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual DateTime RentalDate { get; set; }
        public virtual Rating Rating { get; set; }

        private DateTime _dueDate;
        public virtual DateTime DueDate
        {
            get {return _dueDate;}
            set
            {
                if (value.CompareTo(RentalDate) < 0)
                {
                    throw new ArgumentException(
                        string.Format("Invalid attempt to set due date to {0} for rental {1} with rental date {2}; due date cannot be before rental date", value, Id, RentalDate)
                    );                    
                }
                _dueDate = value;
            }
        }

        private DateTime? _returnDate;
        public virtual DateTime? ReturnDate
        { 
            get {return _returnDate;}
            set
            {
                if (value.HasValue)
                {
                    var newReturnDate = value.Value;
                    if (newReturnDate.CompareTo(RentalDate) < 0)
                    {
                        throw new ArgumentException(
                            string.Format(
                                "Invalid attempt to set return date to {0} for rental {1} with rental date {2}; return date cannot be before rental date",
                                newReturnDate, Id, RentalDate)
                            );
                    }
                    _returnDate = newReturnDate;
                }
                else
                {
                    _returnDate = null;
                }
            }
        }
        

        protected internal Rental() : base ()
        {
            RentalDate = DateFactory.CurrentDate;
        }

        public Rental(Customer customer, Video video) : this ()
        {
            Customer = customer;
            Video = video;

            if (video.NewArrival)
            {
                DueDate = RentalDate.AddDays(7);
            }
            else
            {
                DueDate = RentalDate.AddDays(3);
            }
        }
    }
}