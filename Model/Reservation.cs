﻿namespace Model
{
    public class Reservation
    {
        public virtual Customer Customer { get; set; }
        public virtual Movie Movie { get; set; }
    }
}