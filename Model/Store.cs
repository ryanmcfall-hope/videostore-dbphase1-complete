using System;
using System.Collections.Generic;

namespace Model
{
    public class Store : BaseEntity
    {
        public virtual string StreetAddress { get; set; }
        public virtual ZipCode ZipCode { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual IList<Employee> Managers { get; protected internal set; }
        public virtual IList<Video> Videos { get; protected internal set; }

        public Store() : base()
        {
            Managers = new List<Employee>(8);
            Videos = new List<Video>(256);
        }

        public virtual void AddManager(Employee newManager)
        {
            Managers.Add(newManager);
        }

        public virtual void RemoveManager(Employee manager)
        {
            Managers.Remove(manager);
        }

        public virtual void AddVideo(Video video)
        {
            Videos.Add(video);
        }

        public virtual void RemoveVideo(Video video)
        {
            if (!Videos.Remove(video))
            {
                var message = string.Format(
                    "Attempted to remove video with ID {0} from store with ID {1}; the store does not own this video",
                    video.Id, this.Id
                );
                throw new ArgumentException(message);
            }
        }
    }
}