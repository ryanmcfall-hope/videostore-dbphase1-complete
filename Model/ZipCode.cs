﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class ZipCode
    {
        public virtual string Code { get; set; }
        public virtual string City { get; set; }
        public virtual string State { get; set; }

        public ZipCode() 
        {
            
        }
        public override bool Equals(object obj)
        {
            var other = obj as ZipCode;
            if (other == null) return false;

            return other.Code.Equals(Code);
        }

        public override int GetHashCode()
        {
            return Code.GetHashCode();
        }
    }
}
