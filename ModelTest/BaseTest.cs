﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace ModelTest
{
    public abstract class BaseTest
    {
        protected Movie hoosiers;
        protected Video hoosiersVideo;
        protected Movie sleepless;
        protected Video sleeplessVideo;
        protected Movie youveGotMail;
        protected Video youveGotMailVideo;

        protected Store chicagoDriveStore;
        protected Customer me;
        protected ZipCode _holland;
        protected ZipCode _holland2;
        protected ZipCode _zeeland;
        protected Area hollandArea;

        protected CommunicationMethod email;
        protected CommunicationMethod postalMail;

        [TestInitialize]
        public void SetUp()        
        {
            DateFactory.Mode = DateFactoryMode.Test;

            _holland = new ZipCode() { Code = "49423", City = "Holland", State = "Michigan" };
            _holland2 = new ZipCode() { Code = "49423", City = "Holland", State = "Michigan" };
            _zeeland = new ZipCode() { Code = "49464", City = "Zeeland", State = "Michigan" };

            chicagoDriveStore = new Store()
            {
                
            };

            me = new Customer
            {
                Name = new Name() {Title = "Dr.", First = "Ryan", Middle = "L", Last = "McFall"}
            };

            hoosiers = new Movie()
            {
                MovieTitle = "Hoosiers", Year = 1986
            };

            hoosiersVideo = new Video()
            {
                Id = 1,
                Movie = hoosiers,
                Store = chicagoDriveStore
            };

            sleepless = new Movie() {MovieTitle = "Sleepless in Seattle", Year = 1993};
            sleeplessVideo = new Video() {Id = 2, Movie = sleepless, Store = chicagoDriveStore};

            youveGotMail = new Movie() {MovieTitle = "You've got mail", Year = 1998};
            youveGotMailVideo = new Video() {Id = 3, Movie = youveGotMail, Store = chicagoDriveStore};

            //  Note use of "special" constructor designed for testing
            hollandArea = new Area() {ZipCodes = new HashSet<ZipCode>()};
            hollandArea.ZipCodes.Add(_holland);
            hollandArea.ZipCodes.Add(_holland2);

            email = new CommunicationMethod()
            {
                Name = "Email",
                Frequency = 1,
                Units = TimeUnit.Day
            };

            postalMail = new CommunicationMethod()
            {
                Name = "Postal Mail",
                Frequency = 2,
                Units = TimeUnit.Month,
                Customers = new HashSet<Customer>()
            };            
            postalMail.Customers.Add(me);

        }
    }
}
