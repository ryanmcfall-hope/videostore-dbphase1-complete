﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace ModelTest
{
    [TestClass]
    public class TestArea : BaseTest
    {
        [TestMethod]
        public void ZipCodesAreInitializedByConstructor()
        {
            var newArea = new Area();
            Assert.IsNotNull(newArea.ZipCodes);
            Assert.AreEqual(0, newArea.ZipCodes.Count);
        }

        [TestMethod]
        public void RemoveZipCodeForExistingZipCodeWorksCorrectly()
        {
            hollandArea.RemoveZipCode(_holland);
            //  Using foreach like this doesn't depend on correctness of Equals
            //  operator for ZipCode
            //  Note I could have used the Any extension method that is used in the Add test
            //  below as well
            foreach (var zipCode in hollandArea.ZipCodes)
            {
                Assert.IsFalse(zipCode.Code.Equals(_holland.Code));
            }
        }

        [TestMethod]
        [ExpectedException(typeof (ArgumentException))]
        public void RemoveZipCodeThrowsExceptionForNonExistentZipCode()
        {
            hollandArea.RemoveZipCode(_zeeland);
        }

        [TestMethod]
        public void AddZipCodeAddsZipCodeToCollection()
        {
            int previousCount = hollandArea.ZipCodes.Count;
            hollandArea.AddZipCode(_zeeland);

            Assert.AreEqual(previousCount+1, hollandArea.ZipCodes.Count);

            Assert.IsTrue(hollandArea.ZipCodes.Any(z => z.Code.Equals(_zeeland.Code)));
        }
    }
}
