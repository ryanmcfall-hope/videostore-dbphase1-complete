﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace ModelTest
{
    [TestClass]
    public class TestCommunicationMethod : BaseTest
    {
        [TestMethod]
        public void CustomersShouldBeInitializedInConstructor()
        {
            Assert.IsNotNull(email.Customers);
            Assert.AreEqual(0, email.Customers.Count);
        }

        [TestMethod]
        public void EqualsWithNonCommunicationMethodShouldReturnFalse()
        {
            Assert.AreNotEqual(email, "Email");
        }

        [TestMethod]
        public void EqualsWithNullShouldReturnFalse()
        {
            Assert.AreNotEqual(email, null);
        }

        [TestMethod]
        public void EqualsWithTwoDifferentCommunicationMethodsShouldReturnFalse()
        {
            Assert.AreNotEqual(email, postalMail);
        }

        [TestMethod]
        public void EqualsWithTwoCommunicationMethodsWithSameNameShouldReturnTrue()
        {
            var other = new CommunicationMethod() {Name = email.Name};

            Assert.AreEqual(email, other);
        }

        [TestMethod]
        public void EqualsOnSameCommunicationMethodShouldReturnTrue()
        {
            Assert.AreEqual(email, email);
        }

        [TestMethod]
        public void HashCodeReturnsHasCodeOfNameProperty()
        {
            Assert.AreEqual(email.GetHashCode(), email.Name.GetHashCode());
        }
    }
    
}
