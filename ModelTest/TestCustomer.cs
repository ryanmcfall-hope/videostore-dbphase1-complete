﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace ModelTest
{
    [TestClass]
    public class TestCustomer : BaseTest
    {
        [TestMethod]
        public void EmptyConstructorInitializesCollections()
        {
            var cust = new Customer();
            Assert.IsNotNull(cust.Rentals);
            Assert.AreEqual(0, cust.Rentals.Count);

            Assert.IsNotNull(cust.CommunicationTypes);
            Assert.AreEqual(0, cust.CommunicationTypes.Count);

            Assert.IsNotNull(cust.PreferredStores);
            Assert.AreEqual(0, cust.PreferredStores.Count);

        }

        [TestMethod]
        public void ValidPasswordIsAccepted()
        {
            var password = "aA1bB1";
            me.Password = password;
            Assert.AreEqual(password, me.Password);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Password is too short.  It must be at least 6 characters long.")]
        public void ShortPasswordIsRejected()
        {
            var password = "1a2Bb";
            me.Password = password;
        }

        [ExpectedException(typeof(ArgumentException), "The password must contain at least 1 number.")]
        [TestMethod]
        public void PasswordWithoutNumberIsRejected()
        {
            var password = "aaBaCa";
            me.Password = password;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "The password must contain at least 1 lowercase letter.")]
        public void PasswordWithoutLowercaseIsRejected()
        {
            var password = "AB123C2D";
            me.Password = password;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "The password must contain at least 1 uppercase letter.")]
        public void PasswordWithoutUppercaseIsRejected()
        {
            var password = "ab123c2d";
            me.Password = password;
        }

        [TestMethod]
        public void AllowAddsCustomerToCommMethodList()
        {
            me.Allow(email);
            Assert.IsTrue(CommunicationTypeContainsCustomer(email, me));
        }

        [TestMethod]
        public void DenyRemovesCustomerFromCommMethodList()
        {
            postalMail.Customers = new HashSet<Customer>();
            postalMail.Customers.Add(me);
            me.CommunicationTypes = new HashSet<CommunicationMethod>();
            me.CommunicationTypes.Add(postalMail);

            //  Customer "me" has been added to the list through the CommunicationMethod
            //  constructor designed for that purpose
            me.Deny(postalMail);

            Assert.IsFalse(CommunicationTypeContainsCustomer(postalMail, me));
        }

        [TestMethod]
        public void DenyRemovesCommMethodFromCustomerCommunicationTypes()
        {
            me.CommunicationTypes = new HashSet<CommunicationMethod>();
            me.CommunicationTypes.Add(email);            
            me.Deny(email);

            Assert.IsFalse(CommunicationTypeContainsCustomer(email, me));
        }

        [TestMethod]
        public void FullNameReturnsCorrectValue()
        {
            Assert.AreEqual("Ryan McFall", me.FullName);    

            //  Make sure that changes are taken into account
            me.Name.First = "Someone";
            me.Name.Last = "Else";

            Assert.AreEqual("Someone Else", me.FullName);
        }

        [TestMethod]
        public void AddReservationReturnsAppropriateReservation()
        {
            var returnedReservation = me.AddReservation(hoosiers);
            Assert.AreSame(hoosiers, returnedReservation.Movie);
            Assert.AreSame(me, returnedReservation.Customer);
        }

        [TestMethod]
        public void AddReservationSetsReservationPropertyForCustomer()
        {
            var returnedReservation = me.AddReservation(hoosiers);
            Assert.AreSame(returnedReservation, me.Reservation);
        }

        [TestMethod]
        public void AddReservationAddsCustomerToReservationsList()
        {
            var returnedReservation = me.AddReservation(hoosiers);
            Assert.IsTrue(hoosiers.Reservations.Any(r => r == returnedReservation));
        }

        [TestMethod]
        public void RentMethodReturnsRentalWithCorrectProperties()
        {
            var rental = me.Rent(hoosiersVideo);

            Assert.AreSame(me, rental.Customer);
            Assert.AreSame(hoosiersVideo, rental.Video);
        }

        [TestMethod]
        public void RentMethodAddsRentalToListOfRentals()
        {
            var rental = me.Rent(hoosiersVideo);
            Assert.IsTrue(me.Rentals.Any(r => r.Customer == me && r.Video == hoosiersVideo));
        }

        [TestMethod]
        public void LateRentalsDoesNotIncludeReturnedRentals()
        {
            var rental = me.Rent(hoosiersVideo);
            rental.DueDate = DateTime.Now.AddDays(-1);
            rental.ReturnDate = rental.DueDate.AddDays(-1);
            var lateRentals = me.LateRentals();
            Assert.AreEqual(0, lateRentals.Count);
        }

        [TestMethod]
        public void LateRentalsIncludeCorrectRentals()
        {
            var hoosiersRental = me.Rent(hoosiersVideo);
            var sleeplessRental = me.Rent(sleeplessVideo);
            var youveGotMailRental = me.Rent(youveGotMailVideo);
            hoosiersRental.DueDate = DateTime.Now.AddDays(-2);
            youveGotMailRental.DueDate = DateTime.Now.AddDays(-3);
            sleeplessRental.DueDate = DateTime.Now.AddDays(3);

            var lateRentals = me.LateRentals();
            Assert.AreEqual(2, lateRentals.Count);
            Assert.IsTrue(lateRentals.Any(r => r.Video.Movie.MovieTitle.Equals(hoosiers.MovieTitle)));
            Assert.IsTrue(lateRentals.Any(r => r.Video.Movie.MovieTitle.Equals(youveGotMail.MovieTitle)));
        }

        [TestMethod]
        public void LateRentalsAreInDescendingOrderByDaysOverdue()
        {
            var hoosiersRental = me.Rent(hoosiersVideo);
            var sleeplessRental = me.Rent(sleeplessVideo);
            var youveGotMailRental = me.Rent(youveGotMailVideo);
            hoosiersRental.DueDate = DateTime.Now.AddDays(-2);
            youveGotMailRental.DueDate = DateTime.Now.AddDays(-3);
            sleeplessRental.DueDate = DateTime.Now.AddDays(-1);
            var lateRentals = me.LateRentals();
            Assert.AreEqual(3, lateRentals.Count);

            Assert.IsTrue(lateRentals[0].DueDate < lateRentals[1].DueDate);
            Assert.IsTrue(lateRentals[1].DueDate < lateRentals[2].DueDate);            
        }

        private static bool CommunicationTypeContainsCustomer(CommunicationMethod method, Customer customer)
        {
            return method.Customers.Any(
                c => c.Name.First.Equals(customer.Name.First) && c.Name.Last.Equals(customer.Name.Last)
            );
        }
    }
}
