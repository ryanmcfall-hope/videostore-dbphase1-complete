﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace ModelTest
{
    [TestClass]
    public class TestEmployee : BaseTest
    {
        private Employee _subordinate;
        private Employee _supervisor;
        private Employee _otherManager;

        [TestInitialize]
        public void CreateEmployees ()
        {            
            _subordinate = new Employee() {Name = new Name() {First = "Sub", Last = "Ordinate", }, Id = 1};
            _supervisor = new Employee() {Name = new Name() {First = "Super", Last = "Visor"}, Id = 2};
            _otherManager = new Employee() {Name = new Name() {First = "Other", Last = "Manager"}, Id = 3};            
        }

        [TestMethod]
        public void DateHiredInitializedToCurrentDateInConstructor()
        {
            Assert.AreEqual(DateFactory.TestDate, _subordinate.DateHired);
        }

        [TestMethod]
        public void IsManagerReturnsTrueForManagerOfStore()
        {
            chicagoDriveStore.Managers = new List<Employee>() {_subordinate, _otherManager};
            _subordinate.Store = chicagoDriveStore;
            _otherManager.Store = chicagoDriveStore;
            
            Assert.IsTrue(_subordinate.IsManager);
        }

        [TestMethod]
        public void IsManagerReturnsFalseForEmployeeWhoIsNotAManager()
        {
            chicagoDriveStore.Managers = new List<Employee>() {_otherManager};
            _otherManager.Store = chicagoDriveStore;
            _subordinate.Store = chicagoDriveStore;
            Assert.IsFalse(_subordinate.IsManager);
        }

        [TestMethod]
        public void SetSupervisorToOtherEmployeeIsSuccessful()
        {
            _subordinate.Supervisor = _supervisor;
            Assert.AreSame(_supervisor, _subordinate.Supervisor);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SetSupervisorToSelfFails()
        {
            _subordinate.Supervisor = _subordinate;
        }

        [TestMethod]
        public void BirthDateCanBeSetToValidValue()
        {
            var sixteenYearsOld = DateFactory.TestDate.AddYears(16);
            _subordinate.BirthDate = sixteenYearsOld;
            Assert.AreEqual(sixteenYearsOld, _subordinate.BirthDate);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void BirthDateCannotBeSetToValueMakingEmployeeUnderAgeSixteen()
        {
            var sixteenYearsOld = DateFactory.TestDate.AddYears(16);
            _subordinate.BirthDate = sixteenYearsOld;

            var underSixteen = DateFactory.TestDate.AddYears(16).AddSeconds(-1);
            _subordinate.BirthDate = underSixteen;
        }
    }
}
