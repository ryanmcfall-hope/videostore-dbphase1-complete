﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace ModelTest
{
    [TestClass]
    public class TestGenre
    {
        [TestMethod]
        public void TestGenreEquals()
        {
            var comedy1 = new Genre() {Name = "Comedy"};
            var comedy2 = new Genre() {Name = "Comedy"};
            var drama = new Genre() {Name = "Drama"};

            var comedy = "Comedy";

            Assert.AreEqual(comedy1, comedy2, "Two different Genre objects with the same Name property should be considered equal");
            Assert.AreNotEqual(comedy1, drama, "Two genres with different Name properties should not be considered equal");

            Assert.AreNotEqual(comedy1, comedy, "A genre and a string with the same contents should not be considered equal");
        }

        [TestMethod]
        public void TestGenreGetHashCode()
        {
            var comedy = "Comedy";
            var genre = new Genre() {Name = comedy};

            Assert.AreEqual(comedy.GetHashCode(), genre.GetHashCode(), "Genre GetHashCode should return hash code of the Name property");
        }
    }
}
