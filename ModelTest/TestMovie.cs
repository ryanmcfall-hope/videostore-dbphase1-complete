﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace ModelTest
{
    [TestClass]
    public class TestMovie : BaseTest
    {
        [TestMethod]
        public void ReservationsInitializedInConstructor()
        {
            var movie = new Movie();
            Assert.IsNotNull(movie.Reservations, "Reservations should be initialized in Movie constructor");
            Assert.AreEqual(0, movie.Reservations.Count);
        }

        [TestMethod]
        public void AddReservationReturnsCorrectReservation()
        {
            var reservation = hoosiers.AddReservation(me);

            Assert.AreSame(hoosiers, reservation.Movie);
            Assert.AreSame(me, reservation.Customer);
        }

        [TestMethod]
        public void AddReservationAddsReservationToMovie()
        {
            var reservation = hoosiers.AddReservation(me);
            Assert.AreEqual(1, hoosiers.Reservations.Count);
            Assert.AreSame(reservation, hoosiers.Reservations[0]);
        }

        [TestMethod]
        public void AddReservationSetsCustomerReservationProperty()
        {
            var reservation = hoosiers.AddReservation(me);
            Assert.AreSame(reservation, me.Reservation);
        }

    }
}
