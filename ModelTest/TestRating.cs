﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace ModelTest
{
    [TestClass]
    public class TestRating : BaseTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ScoreBelowMinimumIsRejectedInConstructor()
        {
            var rating = new Rating(Rating.MinScore - 1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ScoreAboveMaximumIsRejectedInConstructor()
        {
            var rating = new Rating(Rating.MaxScore + 1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ScoreBelowMinimumIsRejectedInSetter()
        {
            var rating = new Rating(Rating.MinScore);
            rating.Score--;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ScoreAboveMaximumIsRejectedInSetter()
        {
            var rating = new Rating(Rating.MaxScore);
            rating.Score++;
        }

        [TestMethod]
        public void ValidScoresAreAllowed()
        {
            var rating = new Rating(Rating.MinScore);
            rating = new Rating(Rating.MaxScore);
        }
    }
}
