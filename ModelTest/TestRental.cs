﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace ModelTest
{
    [TestClass]
    public class TestRental : BaseTest
    {        
        [TestMethod]
        public void RentalDateInitializedInConstructor()
        {
            var rental = new Rental(me, sleeplessVideo);
            Assert.AreEqual(DateFactory.TestDate, rental.RentalDate);
        }

        [TestMethod]
        public void DueDateSetCorrectlyForNewRelease()
        {
            sleeplessVideo.NewArrival = true;
            var rental = new Rental(me, sleeplessVideo);
            Assert.AreEqual(DateFactory.TestDate.AddDays(7), rental.DueDate);
        }

        [TestMethod]
        public void DueDateSetCorrectlyForOldRelease()
        {
            sleeplessVideo.NewArrival = false;
            var rental = new Rental(me, sleeplessVideo);
            Assert.AreEqual(DateFactory.TestDate.AddDays(3), rental.DueDate);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ReturnDateCannotBeSetBeforeRentalDate()
        {            
            var rental = new Rental(me, sleeplessVideo);
            rental.ReturnDate = rental.RentalDate.AddTicks(-1);            
        }

        [TestMethod]
        [ExpectedException(typeof (ArgumentException))]
        public void DueDateCannotBeSetBeforeRentalDate()
        {
            var rental = new Rental(me, sleeplessVideo);
            rental.DueDate = rental.RentalDate.AddTicks(-1);            
        }
    }
}
