﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace ModelTest
{
    [TestClass]
    public class TestStore : BaseTest
    {
        [TestMethod]
        public void CollectionsInitializedInConstructor()
        {
            Assert.IsNotNull(chicagoDriveStore.Managers);
            Assert.AreEqual(0, chicagoDriveStore.Managers.Count);

            Assert.IsNotNull(chicagoDriveStore.Videos);
            Assert.AreEqual(0, chicagoDriveStore.Videos.Count);
        }

        [TestMethod]
        public void AddManagerAddsCorrectly()
        {
            var employee = new Employee()
            {
                Name = new Name() {First = "A", Last = "Manager"}
            };

            chicagoDriveStore.AddManager(employee);

            Assert.AreEqual(1, chicagoDriveStore.Managers.Count);
            Assert.IsTrue(ManagerExistsForStore(chicagoDriveStore, employee));
        }

        [TestMethod]
        public void RemoveManagerRemovesCorrectly()
        {
            var manager1 = new Employee()
            {
                Name = new Name() {First = "A", Last = "Manager"}
            };
            var manager2 = new Employee()
            {
                Name = new Name() {First = "Another", Last = "Manager"}
            };
            var managers = new Employee[] {manager1, manager2};

            var store = new Store() {Managers = new List<Employee>() {manager1, manager2}};
            
            store.RemoveManager(manager1);
            Assert.IsFalse(ManagerExistsForStore(store, manager1));
            Assert.IsTrue(ManagerExistsForStore(store, manager2));
        }

        [TestMethod]
        public void AddVideoAddsCorrectly()
        {
            chicagoDriveStore.AddVideo(sleeplessVideo);
            Assert.IsTrue(VideoExistsForStore(chicagoDriveStore, sleeplessVideo));
        }

        [TestMethod]
        public void RemoveVideoForExistingVideoRemovesCorrectly()
        {            
            var store = new Store() {Videos = new List<Video>() {sleeplessVideo, hoosiersVideo, youveGotMailVideo}};
            store.RemoveVideo(hoosiersVideo);
            Assert.IsFalse(VideoExistsForStore(store, hoosiersVideo));
            Assert.IsTrue(VideoExistsForStore(store, sleeplessVideo));
            Assert.IsTrue(VideoExistsForStore(store, youveGotMailVideo));
        }

        [TestMethod]
        [ExpectedException(typeof (ArgumentException))]
        public void RemoveVideoThatStoreDoesNotOwnThrowsException()
        {
            chicagoDriveStore.RemoveVideo(hoosiersVideo);
        }

        private static bool VideoExistsForStore(Store store, Video video)
        {
            return store.Videos.Any(v => v.Movie.MovieTitle.Equals(video.Movie.MovieTitle));
        }

        private static bool ManagerExistsForStore(Store store, Employee manager)
        {
            return store.Managers.Any(
                emp => emp.Name.First.Equals(manager.Name.First) && emp.Name.Last.Equals(manager.Name.Last)
            );
        }
    }
}
