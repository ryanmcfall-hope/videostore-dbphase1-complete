﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace ModelTest
{
    [TestClass]
    public class TestVideo : BaseTest
    {               
        [TestMethod]
        public void DateInitializedInConstructor()
        {
            
        }

        [TestMethod]
        public void NewArrivalDefaultsToTrue()
        {
            Assert.IsTrue(hoosiersVideo.NewArrival, "New arrival should default to true for a video");
        }
    }
}
