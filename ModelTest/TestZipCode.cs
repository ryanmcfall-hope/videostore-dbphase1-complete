﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace ModelTest
{
    [TestClass]
    public class TestZipCode : BaseTest
    {        
        [TestMethod]
        public void TestGetHashCodeReturnsCodePropertysHashCode()
        {
            var expectedHashCode = _holland.Code.GetHashCode();
            Assert.AreEqual(expectedHashCode, _holland.GetHashCode());
        }

        [TestMethod]
        public void TestEqualityOperatorOnDifferentZipCodesReturnsFalse()
        {
            Assert.IsFalse(_holland.Equals(_zeeland), "Two zip codes with different Code properties should not be considered equal");
        }

        [TestMethod]
        public void TestEqualityOperatorOnDifferentTypesReturnsFalse()
        {
            Assert.IsFalse(_holland.Equals("49423"), "Comparing a zip code to a string with same Code value should not be considered equal");
        }

        [TestMethod]
        public void TestEqualityOnSameObjectReturnsTrue()
        {
            Assert.IsTrue(_holland.Equals(_holland), "Comparing Equality of the same ZipCode object to itself should return true");
        }

        [TestMethod]
        public void TestEqualityOnTwoZipCodesWithSameCodePropertyReturnsTrue()
        {
            Assert.IsTrue(_holland.Equals(_holland2), "Comparing a ZipCode object to another ZipCode object with the same Code property should return true");
        }
    }
}
